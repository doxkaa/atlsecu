# Atelier Sécurisation

**<u>Membres du groupe :</u>** Louis Ducruet, Karl Lechartier, Nathan Picot

## Présentation de l'infrastructure

![](docs/img/schema_RZO.png)

L'infrastructure est segmentée en 2 sous réseaux pour permettre de bloquer des flux au niveau du firewall.

- 10.40.X.1 : firewall (opnsense 24)
- 10.40.1.2 : web (debian 12 avec apache2)
- 10.40.0.2 : bdd (debian 12 avec mariadb)
- 10.40.0.3 : déploiement (debian 12 avec ansible)

Une entrée DNS (nom de domaine du site WordPress) est présente sur l'IP publique du firewall `atlsecu.com` et `www.atlsecu.com`

## Firewall : Règles de filtrage

**<u>NOTE :</u>** Ces règles sont à la fois configurées sur le firewall/routeur (OPNsense) et sur chaque machine avec le paquet `ufw` qui est un firewall. De cette manière même si un des équipements est altéré, les règles de filtrage continue de s'appliquer. 
**De plus, tout ce qui n'est pas explicitement autorisé est bloqué.**

### WAN

On autorise le port 80 et 443 à destination du serveur WEB. Nous avons du DNAT des ports 80 et 443 depuis l'interface WAN du firewall vers l'interface du serveur web.

![](docs/img/firewall_wan.png)

### DMZ

On autorise les ports 80, 443 et 53 vers l'extérieur. On autorise également le serveur web a communiquer avec la bdd seulement sur le port 3306.

![](docs/img/firewall_dmz.png)

### LAN

On autorise les services web et dns en sortie ainsi que le port 22 depuis la machine de déploiement vers tous les autres serveurs.

![](docs/img/firewall_lan.png)

## fail2ban : limitation des requêtes d'un utilisateur

Pour lutter contre les tentatives répétées d'intrusion, `fail2ban` est configuré sur les machines web et base de données. Cette détection est couplé à `ufw` pour bloquer les requêtes malveillantes.

### Règle commune

Pour ssh, si il y a plus de 5 tentative en 10 minutes on effectue un blocage pour 1 heure

![](docs/img/fail2ban_sshd.png)

### Règles d'apache2

Bloquer si il y a plus de 100 requêtes en 30 secondes (soit 3,3/s) en méthode GET

Bloquer si il y a plus de 60 requêtes en 29 secondes (soit 2/s) en méthode POST

Utilisation des filtres prédéfinie par fail2ban pour apache2 :

- détection des robots 
- détection de recherche d'exploit
- détection des tentatives d'overflow

Boucle de curl avec la fin du retour de la dernière requêtes valide et le refus des suivantes :
![](docs/img/fail2ban_apache2_client.png)
![](docs/img/fail2ban_apache2_server.png)

## Web : Configuration et sécurisation

### Apache2

On commence sur le serveur web, par l'installation d'apache2 et php8.2 nécessaire pour WordPress.

Apache a été configurer de manière à masquer sa version à la fois dans les en-tête et sur les pages d'erreur. 

![](docs/img/apache2_version.png)

### Mod_Security

Installation et configuration de libapache2-mod-security2 pour servir de WAF.

On a configuré pour qu'il applique les règles prédéfinis par WordPress au niveau du blocage suite à une détection d'attaque. (si des règles pour WordPress ne sont pas mise en place, il n'est notamment pas possible de se connecter)

![](docs/img/waf_demo.png)

### Installation et configuration de WordPress

L'installation de WordPress et sa configuration y compris des tokens de sécurité sont géré par Ansible, il ne reste sur le site plus que la configuration du compte admin et le nom du site a effectuer sur l'interface graphique.

Le playbook s'occupe également de la génération de certificat auto-signé (dans un cas concret, il faudrait utiliser un certificat valide) pour le site WordPress afin d'utiliser le protocole https au lieu du protocole http. Dans ce but, une redirection est en place de http vers https.

![](docs/img/wordpress.png)

### Ajout de plugins WordPress

Pour éviter les attaques de cross-site scripting, on a mis en place une sécurité via WordPress avec l'ajout d'une extension nommée "Headers Security Advanced & HSTS WP". Cette extension implémente des en-têtes de réponse HTTP que votre site peut utiliser pour augmenter la sécurité de votre site Web. 

Pour le Cross-Origin Resource Sharing (CORS), on a configuré une extension WordPress qui se nomme "Enable CORS". Cette extension empêche les pages Web d'effectuer des requêtes vers un domaine différent de celui qui a servi la page Web.

![](docs/img/CORS.png)

Pour la définition d'en-têtes sécurisés, on a configuré une extension qui se nomme "En-tête HTTP". Grâce à cette extension, on peut gérer la sécurité de nos en-têtes avec de multiples options de sécurité. Voici les options que nous avons mises en place.

![](docs/img/HTTP.png)

## Base de donnée : Configuration et sécurisation

La base de donnée est configuré pour écouter sur 0.0.0.0 mais le firewall au niveau de la machine `ufw` bloque toute les requêtes qui ne sont pas en provenance du serveur web.

Un utilisateur propre à la base de données est défini avec tout les droit uniquement sur la base et n'étant utilisable que depuis l'ip du serveur web.

# Scan du site

Un scan du site a été effectué avec l'outil ZAP [site web](https://www.zaproxy.org/). 

**<u>NOTE :</u>** Pour la réalisation du scan, la protection anti dos a été désactivée pour qu'il puisse scanner l'ensemble du site sans être bloqué.

Le détail du scan est disponible dans le dossier docs [ici](docs/2024-04-11-ZAP-Report.pdf)

Les alertes concernant CSP script-src et style-src sont causées par l'autorisation du inline car le thème l'utilise.
